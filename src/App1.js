import React, { Component } from "react";
import {useNavigate} from "react-router-dom"
import logo from "./assets/loginn1.png";
import logo1 from "./assets/Logo.svg";
import "./App.css";
import dashboard from './dashboard';


class App extends Component {

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(e.target.email.value);

    if (!e.target.email.value) {
      alert("Email is required");
    } else if (!e.target.email.value) {
      alert("Valid email is required");
    } else if (!e.target.password.value) {
      alert("Password is required");
    } else if (
      e.target.email.value === "me@example.com" &&
      e.target.password.value === "123456"
    ) {
      alert("Successfully logged in");
      e.target.email.value = "";
      e.target.password.value = "";

    } else {
      alert("Wrong email or password combination");
    }
  };



  handleClick = (e) => {
    e.preventDefault();
    alert("I am here")

    /*
    const response =  await fetch(
        "http://localhost:9000/loginUser"
      ).then((response) => response.json());

      alert(JSON.stringify(response))
    */

    const url = 'http://localhost:9000/loginUser';// Axios
    axios.get(url)
      .then(response => alert(response));// Fetch

    fetch(url)
      .then(response => response.json())
      .then(data => console.log(data));

    const navigate = useNavigate();
    //navigate("../dashboard");
    /*
    const navigate = useNavigation();
    navigate("/redirect-example", { replace: true });
    */
    navigate("../dashboard", { replace: true });

  };

  render() {
    return (

      <div class="wrap">
          <div class="header">
            <div id="outer">
              <div class="div"></div>
              <div class="div"></div>
          </div>
      </div>
          <div class="nav" align="center"><b>LOGIN</b></div>
      		<div class="container">
            <div class="split left">
              <div class="centered">
              <img src={logo} className="logo" alt="eShoppe" />
              </div>
            </div>

            <div class="split right" >
            <form className="form" onSubmit1={this.handleSubmit}>
              <div className="input-group">
                <label htmlFor="email">Email</label>
                <input type="email" name="email" placeholder="test@email.com" />
              </div>
              <div className="input-group">
                <label htmlFor="password">Password</label>
                <input type="password" name="password" />
              </div>
              <button className="primary" onClick={this.handleClick}>Sign In</button>
            </form>
            </div>
          </div>
      		<div class1="footer"></div>
      </div>
    );
  }
}

export default App;
